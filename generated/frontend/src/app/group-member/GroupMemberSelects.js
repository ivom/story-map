import React from 'react'
import { groupLabel } from '../group/GroupSelects'
import { personLabel } from '../person/PersonSelects'
import { AsyncSelect } from '../../shared'
import { listGroupMembers, useGroupMember } from './group-member-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const groupMemberLabel = data => data && entityLabel(', ',
  groupLabel(data.group),
  personLabel(data.person),
  data.description)

export const GroupMemberSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listGroupMembers({ group: query })}
               getOptionValue={option => option.id}
               getOptionLabel={groupMemberLabel}
               restoredValue={useRestored(name + 'Id', useGroupMember)}
               name={name}
               {...rest}/>
