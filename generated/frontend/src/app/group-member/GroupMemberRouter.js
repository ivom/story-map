import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import GroupMemberCreate from './GroupMemberCreate'
import GroupMemberEdit from './GroupMemberEdit'
import GroupMemberDetail from './GroupMemberDetail'
import GroupMemberList from './GroupMemberList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <GroupMemberCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <GroupMemberEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <GroupMemberDetail/>
      </Route>
      <Route path={path}>
        <GroupMemberList/>
      </Route>
    </Switch>
  </>
}
