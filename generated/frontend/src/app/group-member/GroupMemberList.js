import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { groupLabel, GroupSelect } from '../group/GroupSelects'
import { personLabel, PersonSelect } from '../person/PersonSelects'
import { groupMemberFromApi, groupMemberToApi, useGroupMembers } from './group-member-api'

let searchValuesCache = {
  id: '',
  group: '',
  person: '',
  description: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Group members
        <CreateButton to="/group-members/new" title="Create new group member..."
                      initialValues={groupMemberToApi(searchValuesCache)}/>
      </>
    }
    url="/group-members"
    useResourceList={useGroupMembers}
    toApi={groupMemberToApi}
    fromApi={groupMemberFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Group</th>
        <th>Person</th>
        <th>Description</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{groupLabel(item.group)}</td>
        <td>{personLabel(item.person)}</td>
        <td>{item.description}</td>
      </>
    }
  />
