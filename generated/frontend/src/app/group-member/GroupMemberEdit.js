import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { GroupSelect } from '../group/GroupSelects'
import { PersonSelect } from '../person/PersonSelects'
import { updateGroupMember, useGroupMemberEdit } from './group-member-api'

export default () =>
  <EditScreen
    title="Edit group member"
    entityTitle="Group member"
    url="/group-members"
    useResourceEdit={useGroupMemberEdit}
    rows={3}
    validationSchema={
      Yup.object({
        group: Yup.object().nullable()
          .required(),
        person: Yup.object().nullable()
          .required(),
        description: Yup.string()
      })
    }
    update={data => updateGroupMember(data.id, data.version, data)}>

    <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </EditScreen>
