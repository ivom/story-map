import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandGroup } from '../group/group-api'
import { expandPerson } from '../person/person-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.group?.name).localeCompare(String(b.group?.name)))
}

update(data => ({ ...data, groupMembers: data.groupMembers || [] }))

export const expandGroupMember = values => {
  if (values) {
    values = expand(values, 'groupId', 'group', 'groups')
    values.group = expandGroup(values.group)
    values = expand(values, 'personId', 'person', 'people')
    values.person = expandPerson(values.person)
  }
  return values
}

export const groupMemberToApi = values => {
  values = collapse(values, 'group', 'id', 'groupId')
  values = collapse(values, 'person', 'id', 'personId')
  return values
}
export const groupMemberFromApi = values => {
  values = restore(values, 'groupId', 'group', 'id')
  values = restore(values, 'personId', 'person', 'id')
  return values
}

export const listGroupMembers = params => {
  const result = list(params, pageSize, 'groupMembers',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'groupId') &&
      numberMatch(params, item, 'personId') &&
      caseInsensitiveMatch(params, item, 'description')
  )
    .map(expandGroupMember)
    .map(groupMemberFromApi)
  console.log('listGroupMembers', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useGroupMembers = (params, $page = 0, options = {}) =>
  useSWR(['/group-members',
    qs.stringify(params), $page], () => listGroupMembers({ ...params, $page }), { ...defaultSWROptions, ...options })

const getGroupMember = id => {
  const result = groupMemberFromApi(expandGroupMember(getEntity(id, 'groupMembers')))
  console.log('getGroupMember', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useGroupMember = (id, options = {}) =>
  useSWR(`/group-members/${id}`, optionalGet(id, () => getGroupMember(id)), { ...defaultSWROptions, ...options })
export const useGroupMemberEdit = (id, options = {}) =>
  useGroupMember(id, { ...editSWROptions, ...options })

export const createGroupMember = values => {
  const request = groupMemberToApi(values)
  const result = create({
    ...request
  }, 'groupMembers', sort)
  console.log('createGroupMember', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateGroupMember = (id, version, values) => {
  const request = groupMemberToApi(values)
  console.log('updateGroupMember', id, version, request)
  modify(id, version, 'groupMembers', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchGroupMember = (id, version, values) => {
  const request = groupMemberToApi(values)
  console.log('patchGroupMember', id, version, request)
  modify(id, version, 'groupMembers', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
