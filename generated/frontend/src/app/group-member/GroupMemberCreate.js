import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { GroupSelect } from '../group/GroupSelects'
import { PersonSelect } from '../person/PersonSelects'
import { createGroupMember, groupMemberFromApi } from './group-member-api'

export default () =>
  <CreateScreen
    title="Create group member"
    entityTitle="Group member"
    url="/group-members"
    rows={3}
    fromApi={groupMemberFromApi}
    initialValues={{
      group: '',
      person: '',
      description: ''
    }}
    validationSchema={
      Yup.object({
        group: Yup.object().nullable()
          .required(),
        person: Yup.object().nullable()
          .required(),
        description: Yup.string()
      })
    }
    create={createGroupMember}>

    <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </CreateScreen>
