import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { createObject, objectFromApi } from './object-api'

export default () =>
  <CreateScreen
    title="Create object"
    entityTitle="Object"
    url="/objects"
    rows={2}
    fromApi={objectFromApi}
    initialValues={{
      name: '',
      description: ''
    }}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required(),
        description: Yup.string()
      })
    }
    create={createObject}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </CreateScreen>
