import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { objectFromApi, objectToApi, useObjects } from './object-api'

let searchValuesCache = {
  id: '',
  name: '',
  description: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Objects
        <CreateButton to="/objects/new" title="Create new object..."
                      initialValues={objectToApi(searchValuesCache)}/>
      </>
    }
    url="/objects"
    useResourceList={useObjects}
    toApi={objectToApi}
    fromApi={objectFromApi}
    searchFormRows={3}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={3}
    tableHeader={
      <>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.description}</td>
      </>
    }
  />
