import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, DateTimePicker, FieldGroup } from '../../shared'
import { createEvent, eventFromApi } from './event-api'

export default () =>
  <CreateScreen
    title="Create event"
    entityTitle="Event"
    url="/events"
    rows={5}
    fromApi={eventFromApi}
    initialValues={{
      type: '',
      from: '',
      to: '',
      duration: '',
      description: ''
    }}
    validationSchema={
      Yup.object({
        type: Yup.string()
          .required(),
        from: Yup.date().nullable(),
        to: Yup.date().nullable(),
        duration: Yup.string(),
        description: Yup.string()
      })
    }
    create={createEvent}>

    <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={DateTimePicker} name="from" label="From" sm={[2, 9]}/>
    <FieldGroup as={DateTimePicker} name="to" label="To" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="duration" label="Duration" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </CreateScreen>
