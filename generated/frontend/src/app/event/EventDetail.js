import React from 'react'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { useEvent } from './event-api'
import { formatDateTime } from '../../i18n'

export default () =>
  <DetailScreen
    title="Event detail"
    entityTitle="Event"
    rows={6}
    useResourceGet={useEvent}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Event event locations" className="mr-3"
                            to={`/event-locations?eventId=${data.id}`}/>
          <NavigationButton label="Event event people" className="mr-3"
                            to={`/event-people?eventId=${data.id}`}/>
          <NavigationButton label="Event event groups" className="mr-3"
                            to={`/event-groups?eventId=${data.id}`}/>
          <NavigationButton label="Event event objects" className="mr-3"
                            to={`/event-objects?eventId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Type" sm={[2, 10]} value={data.type}/>
        <StaticGroup label="From" sm={[2, 10]} value={formatDateTime(data.from)}/>
        <StaticGroup label="To" sm={[2, 10]} value={formatDateTime(data.to)}/>
        <StaticGroup label="Duration" sm={[2, 10]} value={data.duration}/>
        <StaticGroup label="Description" sm={[2, 10]} value={data.description}/>
      </>
    }
  </DetailScreen>
