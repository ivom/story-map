import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { locationTypeLabel } from '../location-type/LocationTypeSelects'
import { locationLabel } from '../location/LocationSelects'
import { useLocation } from './location-api'

export default () =>
  <DetailScreen
    title="Location detail"
    entityTitle="Location"
    rows={4}
    useResourceGet={useLocation}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Parent locations" className="mr-3"
                            to={`/locations?parentId=${data.id}`}/>
          <NavigationButton label="Location event locations" className="mr-3"
                            to={`/event-locations?locationId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Type" sm={[2, 10]}>
          <Link to={`/location-types/${data.type?.id}`}>
            {locationTypeLabel(data.type)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Value" sm={[2, 10]} value={data.value}/>
        <StaticGroup label="Parent" sm={[2, 10]}>
          <Link to={`/locations/${data.parent?.id}`}>
            {locationLabel(data.parent)}
          </Link>
        </StaticGroup>
      </>
    }
  </DetailScreen>
