import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { LocationTypeSelect } from '../location-type/LocationTypeSelects'
import { LocationSelect } from '../location/LocationSelects'
import { createLocation, locationFromApi } from './location-api'

export default () =>
  <CreateScreen
    title="Create location"
    entityTitle="Location"
    url="/locations"
    rows={3}
    fromApi={locationFromApi}
    initialValues={{
      type: '',
      value: '',
      parent: ''
    }}
    validationSchema={
      Yup.object({
        type: Yup.object().nullable()
          .required(),
        value: Yup.string()
          .required(),
        parent: Yup.object().nullable()
      })
    }
    create={createLocation}>

    <FieldGroup as={LocationTypeSelect} name="type" label="Type" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={Form.Control} name="value" label="Value" sm={[2, 9]} required/>
    <FieldGroup as={LocationSelect} name="parent" label="Parent" sm={[2, 9]}/>

  </CreateScreen>
