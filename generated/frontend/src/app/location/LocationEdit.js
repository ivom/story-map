import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { LocationTypeSelect } from '../location-type/LocationTypeSelects'
import { LocationSelect } from '../location/LocationSelects'
import { updateLocation, useLocationEdit } from './location-api'

export default () =>
  <EditScreen
    title="Edit location"
    entityTitle="Location"
    url="/locations"
    useResourceEdit={useLocationEdit}
    rows={3}
    validationSchema={
      Yup.object({
        type: Yup.object().nullable()
          .required(),
        value: Yup.string()
          .required(),
        parent: Yup.object().nullable()
      })
    }
    update={data => updateLocation(data.id, data.version, data)}>

    <FieldGroup as={LocationTypeSelect} name="type" label="Type" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={Form.Control} name="value" label="Value" sm={[2, 9]} required/>
    <FieldGroup as={LocationSelect} name="parent" label="Parent" sm={[2, 9]}/>

  </EditScreen>
