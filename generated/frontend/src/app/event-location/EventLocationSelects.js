import React from 'react'
import { eventLabel } from '../event/EventSelects'
import { locationLabel } from '../location/LocationSelects'
import { AsyncSelect } from '../../shared'
import { listEventLocations, useEventLocation } from './event-location-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const eventLocationLabel = data => data && entityLabel(', ',
  eventLabel(data.event),
  locationLabel(data.location),
  data.role)

export const EventLocationSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listEventLocations({ event: query })}
               getOptionValue={option => option.id}
               getOptionLabel={eventLocationLabel}
               restoredValue={useRestored(name + 'Id', useEventLocation)}
               name={name}
               {...rest}/>
