import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { eventLabel } from '../event/EventSelects'
import { locationLabel } from '../location/LocationSelects'
import { useEventLocation } from './event-location-api'

export default () =>
  <DetailScreen
    title="Event location detail"
    entityTitle="Event location"
    rows={4}
    useResourceGet={useEventLocation}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Event" sm={[2, 10]}>
          <Link to={`/events/${data.event?.id}`}>
            {eventLabel(data.event)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Location" sm={[2, 10]}>
          <Link to={`/locations/${data.location?.id}`}>
            {locationLabel(data.location)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Role" sm={[2, 10]} value={data.role}/>
      </>
    }
  </DetailScreen>
