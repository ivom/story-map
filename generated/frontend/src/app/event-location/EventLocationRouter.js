import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import EventLocationCreate from './EventLocationCreate'
import EventLocationEdit from './EventLocationEdit'
import EventLocationDetail from './EventLocationDetail'
import EventLocationList from './EventLocationList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <EventLocationCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <EventLocationEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <EventLocationDetail/>
      </Route>
      <Route path={path}>
        <EventLocationList/>
      </Route>
    </Switch>
  </>
}
