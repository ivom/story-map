import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandEvent } from '../event/event-api'
import { expandLocation } from '../location/location-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.event?.type).localeCompare(String(b.event?.type)))
}

update(data => ({ ...data, eventLocations: data.eventLocations || [] }))

export const expandEventLocation = values => {
  if (values) {
    values = expand(values, 'eventId', 'event', 'events')
    values.event = expandEvent(values.event)
    values = expand(values, 'locationId', 'location', 'locations')
    values.location = expandLocation(values.location)
  }
  return values
}

export const eventLocationToApi = values => {
  values = collapse(values, 'event', 'id', 'eventId')
  values = collapse(values, 'location', 'id', 'locationId')
  return values
}
export const eventLocationFromApi = values => {
  values = restore(values, 'eventId', 'event', 'id')
  values = restore(values, 'locationId', 'location', 'id')
  return values
}

export const listEventLocations = params => {
  const result = list(params, pageSize, 'eventLocations',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'eventId') &&
      numberMatch(params, item, 'locationId') &&
      caseInsensitiveMatch(params, item, 'role')
  )
    .map(expandEventLocation)
    .map(eventLocationFromApi)
  console.log('listEventLocations', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventLocations = (params, $page = 0, options = {}) =>
  useSWR(['/event-locations',
    qs.stringify(params), $page], () => listEventLocations({ ...params, $page }), { ...defaultSWROptions, ...options })

const getEventLocation = id => {
  const result = eventLocationFromApi(expandEventLocation(getEntity(id, 'eventLocations')))
  console.log('getEventLocation', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventLocation = (id, options = {}) =>
  useSWR(`/event-locations/${id}`, optionalGet(id, () => getEventLocation(id)), { ...defaultSWROptions, ...options })
export const useEventLocationEdit = (id, options = {}) =>
  useEventLocation(id, { ...editSWROptions, ...options })

export const createEventLocation = values => {
  const request = eventLocationToApi(values)
  const result = create({
    ...request
  }, 'eventLocations', sort)
  console.log('createEventLocation', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateEventLocation = (id, version, values) => {
  const request = eventLocationToApi(values)
  console.log('updateEventLocation', id, version, request)
  modify(id, version, 'eventLocations', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchEventLocation = (id, version, values) => {
  const request = eventLocationToApi(values)
  console.log('patchEventLocation', id, version, request)
  modify(id, version, 'eventLocations', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
