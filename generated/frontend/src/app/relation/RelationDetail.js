import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { personLabel } from '../person/PersonSelects'
import { useRelation } from './relation-api'

export default () =>
  <DetailScreen
    title="Relation detail"
    entityTitle="Relation"
    rows={5}
    useResourceGet={useRelation}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="From" sm={[2, 10]}>
          <Link to={`/people/${data.from?.id}`}>
            {personLabel(data.from)}
          </Link>
        </StaticGroup>
        <StaticGroup label="To" sm={[2, 10]}>
          <Link to={`/people/${data.to?.id}`}>
            {personLabel(data.to)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Type" sm={[2, 10]} value={data.type}/>
        <StaticGroup label="Name" sm={[2, 10]} value={data.name}/>
      </>
    }
  </DetailScreen>
