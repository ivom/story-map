import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { personLabel, PersonSelect } from '../person/PersonSelects'
import { relationFromApi, relationToApi, useRelations } from './relation-api'

let searchValuesCache = {
  id: '',
  from: '',
  to: '',
  type: '',
  name: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Relations
        <CreateButton to="/relations/new" title="Create new relation..."
                      initialValues={relationToApi(searchValuesCache)}/>
      </>
    }
    url="/relations"
    useResourceList={useRelations}
    toApi={relationToApi}
    fromApi={relationFromApi}
    searchFormRows={5}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={PersonSelect} name="from" label="From" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={PersonSelect} name="to" label="To" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={5}
    tableHeader={
      <>
        <th>Id</th>
        <th>From</th>
        <th>To</th>
        <th>Type</th>
        <th>Name</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{personLabel(item.from)}</td>
        <td>{personLabel(item.to)}</td>
        <td>{item.type}</td>
        <td>{item.name}</td>
      </>
    }
  />
