import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import RelationCreate from './RelationCreate'
import RelationEdit from './RelationEdit'
import RelationDetail from './RelationDetail'
import RelationList from './RelationList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <RelationCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <RelationEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <RelationDetail/>
      </Route>
      <Route path={path}>
        <RelationList/>
      </Route>
    </Switch>
  </>
}
