import React from 'react'
import { eventLabel } from '../event/EventSelects'
import { groupLabel } from '../group/GroupSelects'
import { AsyncSelect } from '../../shared'
import { listEventGroups, useEventGroup } from './event-group-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const eventGroupLabel = data => data && entityLabel(', ',
  eventLabel(data.event),
  groupLabel(data.group),
  data.role)

export const EventGroupSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listEventGroups({ event: query })}
               getOptionValue={option => option.id}
               getOptionLabel={eventGroupLabel}
               restoredValue={useRestored(name + 'Id', useEventGroup)}
               name={name}
               {...rest}/>
