import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import EventGroupCreate from './EventGroupCreate'
import EventGroupEdit from './EventGroupEdit'
import EventGroupDetail from './EventGroupDetail'
import EventGroupList from './EventGroupList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <EventGroupCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <EventGroupEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <EventGroupDetail/>
      </Route>
      <Route path={path}>
        <EventGroupList/>
      </Route>
    </Switch>
  </>
}
