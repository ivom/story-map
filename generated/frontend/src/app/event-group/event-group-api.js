import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandEvent } from '../event/event-api'
import { expandGroup } from '../group/group-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.event?.type).localeCompare(String(b.event?.type)))
}

update(data => ({ ...data, eventGroups: data.eventGroups || [] }))

export const expandEventGroup = values => {
  if (values) {
    values = expand(values, 'eventId', 'event', 'events')
    values.event = expandEvent(values.event)
    values = expand(values, 'groupId', 'group', 'groups')
    values.group = expandGroup(values.group)
  }
  return values
}

export const eventGroupToApi = values => {
  values = collapse(values, 'event', 'id', 'eventId')
  values = collapse(values, 'group', 'id', 'groupId')
  return values
}
export const eventGroupFromApi = values => {
  values = restore(values, 'eventId', 'event', 'id')
  values = restore(values, 'groupId', 'group', 'id')
  return values
}

export const listEventGroups = params => {
  const result = list(params, pageSize, 'eventGroups',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'eventId') &&
      numberMatch(params, item, 'groupId') &&
      caseInsensitiveMatch(params, item, 'role')
  )
    .map(expandEventGroup)
    .map(eventGroupFromApi)
  console.log('listEventGroups', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventGroups = (params, $page = 0, options = {}) =>
  useSWR(['/event-groups',
    qs.stringify(params), $page], () => listEventGroups({ ...params, $page }), { ...defaultSWROptions, ...options })

const getEventGroup = id => {
  const result = eventGroupFromApi(expandEventGroup(getEntity(id, 'eventGroups')))
  console.log('getEventGroup', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventGroup = (id, options = {}) =>
  useSWR(`/event-groups/${id}`, optionalGet(id, () => getEventGroup(id)), { ...defaultSWROptions, ...options })
export const useEventGroupEdit = (id, options = {}) =>
  useEventGroup(id, { ...editSWROptions, ...options })

export const createEventGroup = values => {
  const request = eventGroupToApi(values)
  const result = create({
    ...request
  }, 'eventGroups', sort)
  console.log('createEventGroup', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateEventGroup = (id, version, values) => {
  const request = eventGroupToApi(values)
  console.log('updateEventGroup', id, version, request)
  modify(id, version, 'eventGroups', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchEventGroup = (id, version, values) => {
  const request = eventGroupToApi(values)
  console.log('patchEventGroup', id, version, request)
  modify(id, version, 'eventGroups', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
