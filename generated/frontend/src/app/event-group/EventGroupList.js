import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { eventLabel, EventSelect } from '../event/EventSelects'
import { groupLabel, GroupSelect } from '../group/GroupSelects'
import { eventGroupFromApi, eventGroupToApi, useEventGroups } from './event-group-api'

let searchValuesCache = {
  id: '',
  event: '',
  group: '',
  role: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Event groups
        <CreateButton to="/event-groups/new" title="Create new event group..."
                      initialValues={eventGroupToApi(searchValuesCache)}/>
      </>
    }
    url="/event-groups"
    useResourceList={useEventGroups}
    toApi={eventGroupToApi}
    fromApi={eventGroupFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Event</th>
        <th>Group</th>
        <th>Role</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{eventLabel(item.event)}</td>
        <td>{groupLabel(item.group)}</td>
        <td>{item.role}</td>
      </>
    }
  />
