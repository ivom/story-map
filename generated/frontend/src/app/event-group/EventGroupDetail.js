import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { eventLabel } from '../event/EventSelects'
import { groupLabel } from '../group/GroupSelects'
import { useEventGroup } from './event-group-api'

export default () =>
  <DetailScreen
    title="Event group detail"
    entityTitle="Event group"
    rows={4}
    useResourceGet={useEventGroup}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Event" sm={[2, 10]}>
          <Link to={`/events/${data.event?.id}`}>
            {eventLabel(data.event)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Group" sm={[2, 10]}>
          <Link to={`/groups/${data.group?.id}`}>
            {groupLabel(data.group)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Role" sm={[2, 10]} value={data.role}/>
      </>
    }
  </DetailScreen>
