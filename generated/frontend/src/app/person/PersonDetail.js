import React from 'react'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { sentenceCase } from 'change-case'
import { usePerson } from './person-api'

export default () =>
  <DetailScreen
    title="Person detail"
    entityTitle="Person"
    rows={6}
    useResourceGet={usePerson}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Person group members" className="mr-3"
                            to={`/group-members?personId=${data.id}`}/>
          <NavigationButton label="From relations" className="mr-3"
                            to={`/relations?fromId=${data.id}`}/>
          <NavigationButton label="To relations" className="mr-3"
                            to={`/relations?toId=${data.id}`}/>
          <NavigationButton label="Person event people" className="mr-3"
                            to={`/event-people?personId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Family name" sm={[2, 10]} value={data.familyName}/>
        <StaticGroup label="Given names" sm={[2, 10]} value={data.givenNames}/>
        <StaticGroup label="Sex" sm={[2, 10]} value={sentenceCase(data.sex || '')}/>
        <StaticGroup label="Occupation" sm={[2, 10]} value={data.occupation}/>
        <StaticGroup label="Description" sm={[2, 10]} value={data.description}/>
      </>
    }
  </DetailScreen>
