import React from 'react'
import { Form } from 'react-bootstrap'
import { sentenceCase } from 'change-case'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import PersonSexSelect from './PersonSexSelect'
import { personFromApi, personToApi, usePeople } from './person-api'

let searchValuesCache = {
  id: '',
  familyName: '',
  givenNames: '',
  sex: '',
  occupation: '',
  description: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        People
        <CreateButton to="/people/new" title="Create new person..."
                      initialValues={personToApi(searchValuesCache)}/>
      </>
    }
    url="/people"
    useResourceList={usePeople}
    toApi={personToApi}
    fromApi={personFromApi}
    searchFormRows={6}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="familyName" label="Family name" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="givenNames" label="Given names" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={PersonSexSelect} name="sex" label="Sex" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="occupation" label="Occupation" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={6}
    tableHeader={
      <>
        <th>Id</th>
        <th>Family name</th>
        <th>Given names</th>
        <th>Sex</th>
        <th>Occupation</th>
        <th>Description</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{item.familyName}</td>
        <td>{item.givenNames}</td>
        <td>{sentenceCase(item.sex || '')}</td>
        <td>{item.occupation}</td>
        <td>{item.description}</td>
      </>
    }
  />
