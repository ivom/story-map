import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import GroupCreate from './GroupCreate'
import GroupEdit from './GroupEdit'
import GroupDetail from './GroupDetail'
import GroupList from './GroupList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <GroupCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <GroupEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <GroupDetail/>
      </Route>
      <Route path={path}>
        <GroupList/>
      </Route>
    </Switch>
  </>
}
