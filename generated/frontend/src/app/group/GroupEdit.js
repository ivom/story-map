import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { GroupSelect } from '../group/GroupSelects'
import { updateGroup, useGroupEdit } from './group-api'

export default () =>
  <EditScreen
    title="Edit group"
    entityTitle="Group"
    url="/groups"
    useResourceEdit={useGroupEdit}
    rows={2}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required(),
        parent: Yup.object().nullable()
      })
    }
    update={data => updateGroup(data.id, data.version, data)}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={GroupSelect} name="parent" label="Parent" sm={[2, 9]}/>

  </EditScreen>
