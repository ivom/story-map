import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { GroupSelect } from '../group/GroupSelects'
import { createGroup, groupFromApi } from './group-api'

export default () =>
  <CreateScreen
    title="Create group"
    entityTitle="Group"
    url="/groups"
    rows={2}
    fromApi={groupFromApi}
    initialValues={{
      name: '',
      parent: ''
    }}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required(),
        parent: Yup.object().nullable()
      })
    }
    create={createGroup}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={GroupSelect} name="parent" label="Parent" sm={[2, 9]}/>

  </CreateScreen>
