import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { groupLabel, GroupSelect } from '../group/GroupSelects'
import { groupFromApi, groupToApi, useGroups } from './group-api'

let searchValuesCache = {
  id: '',
  name: '',
  parent: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Groups
        <CreateButton to="/groups/new" title="Create new group..."
                      initialValues={groupToApi(searchValuesCache)}/>
      </>
    }
    url="/groups"
    useResourceList={useGroups}
    toApi={groupToApi}
    fromApi={groupFromApi}
    searchFormRows={3}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={GroupSelect} name="parent" label="Parent" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={3}
    tableHeader={
      <>
        <th>Id</th>
        <th>Name</th>
        <th>Parent</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{groupLabel(item.parent)}</td>
      </>
    }
  />
