import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { eventLabel, EventSelect } from '../event/EventSelects'
import { objectLabel, ObjectSelect } from '../object/ObjectSelects'
import { eventObjectFromApi, eventObjectToApi, useEventObjects } from './event-object-api'

let searchValuesCache = {
  id: '',
  event: '',
  object: '',
  role: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Event objects
        <CreateButton to="/event-objects/new" title="Create new event object..."
                      initialValues={eventObjectToApi(searchValuesCache)}/>
      </>
    }
    url="/event-objects"
    useResourceList={useEventObjects}
    toApi={eventObjectToApi}
    fromApi={eventObjectFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={ObjectSelect} name="object" label="Object" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Event</th>
        <th>Object</th>
        <th>Role</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{eventLabel(item.event)}</td>
        <td>{objectLabel(item.object)}</td>
        <td>{item.role}</td>
      </>
    }
  />
