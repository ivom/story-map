import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandEvent } from '../event/event-api'
import { expandObject } from '../object/object-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.event?.type).localeCompare(String(b.event?.type)))
}

update(data => ({ ...data, eventObjects: data.eventObjects || [] }))

export const expandEventObject = values => {
  if (values) {
    values = expand(values, 'eventId', 'event', 'events')
    values.event = expandEvent(values.event)
    values = expand(values, 'objectId', 'object', 'objects')
    values.object = expandObject(values.object)
  }
  return values
}

export const eventObjectToApi = values => {
  values = collapse(values, 'event', 'id', 'eventId')
  values = collapse(values, 'object', 'id', 'objectId')
  return values
}
export const eventObjectFromApi = values => {
  values = restore(values, 'eventId', 'event', 'id')
  values = restore(values, 'objectId', 'object', 'id')
  return values
}

export const listEventObjects = params => {
  const result = list(params, pageSize, 'eventObjects',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'eventId') &&
      numberMatch(params, item, 'objectId') &&
      caseInsensitiveMatch(params, item, 'role')
  )
    .map(expandEventObject)
    .map(eventObjectFromApi)
  console.log('listEventObjects', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventObjects = (params, $page = 0, options = {}) =>
  useSWR(['/event-objects',
    qs.stringify(params), $page], () => listEventObjects({ ...params, $page }), { ...defaultSWROptions, ...options })

const getEventObject = id => {
  const result = eventObjectFromApi(expandEventObject(getEntity(id, 'eventObjects')))
  console.log('getEventObject', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventObject = (id, options = {}) =>
  useSWR(`/event-objects/${id}`, optionalGet(id, () => getEventObject(id)), { ...defaultSWROptions, ...options })
export const useEventObjectEdit = (id, options = {}) =>
  useEventObject(id, { ...editSWROptions, ...options })

export const createEventObject = values => {
  const request = eventObjectToApi(values)
  const result = create({
    ...request
  }, 'eventObjects', sort)
  console.log('createEventObject', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateEventObject = (id, version, values) => {
  const request = eventObjectToApi(values)
  console.log('updateEventObject', id, version, request)
  modify(id, version, 'eventObjects', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchEventObject = (id, version, values) => {
  const request = eventObjectToApi(values)
  console.log('patchEventObject', id, version, request)
  modify(id, version, 'eventObjects', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
