import React from 'react'
import { eventLabel } from '../event/EventSelects'
import { objectLabel } from '../object/ObjectSelects'
import { AsyncSelect } from '../../shared'
import { listEventObjects, useEventObject } from './event-object-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const eventObjectLabel = data => data && entityLabel(', ',
  eventLabel(data.event),
  objectLabel(data.object),
  data.role)

export const EventObjectSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listEventObjects({ event: query })}
               getOptionValue={option => option.id}
               getOptionLabel={eventObjectLabel}
               restoredValue={useRestored(name + 'Id', useEventObject)}
               name={name}
               {...rest}/>
