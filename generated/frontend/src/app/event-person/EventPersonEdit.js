import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { PersonSelect } from '../person/PersonSelects'
import { updateEventPerson, useEventPersonEdit } from './event-person-api'

export default () =>
  <EditScreen
    title="Edit event person"
    entityTitle="Event person"
    url="/event-people"
    useResourceEdit={useEventPersonEdit}
    rows={3}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        person: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    update={data => updateEventPerson(data.id, data.version, data)}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </EditScreen>
