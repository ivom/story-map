import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { eventLabel } from '../event/EventSelects'
import { personLabel } from '../person/PersonSelects'
import { useEventPerson } from './event-person-api'

export default () =>
  <DetailScreen
    title="Event person detail"
    entityTitle="Event person"
    rows={4}
    useResourceGet={useEventPerson}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Event" sm={[2, 10]}>
          <Link to={`/events/${data.event?.id}`}>
            {eventLabel(data.event)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Person" sm={[2, 10]}>
          <Link to={`/people/${data.person?.id}`}>
            {personLabel(data.person)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Role" sm={[2, 10]} value={data.role}/>
      </>
    }
  </DetailScreen>
