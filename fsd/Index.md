# Story map

Capture the facts in any story to:

- Wrap your head around it.
- Connect the dots.
- Crack a whodunnit mystery.
- Convey the gist of the story.

## Data model

- `#Person`
- `#Location`
- `#Group`
- `#Object`
- `#Relation`
- `#Event`


## Entity: Person

### Attributes:

- Family name (O text)
- Given names (O text)
- Sex (O enum: male, female)
- Occupation (O text)
- Description (O text)


## Entity: Location

### Attributes:

- Type (n:1 `#LocationType`)
- Value (M text)
- Parent (n:0..1 `#Location`)


## Entity: Location type

### Attributes:

- Name (M text)


## Entity: Group

### Attributes:

- Name (M text)
- Parent (n:0..1 `#Group`)


## Entity: Group member

### Attributes:

- Group (n:1 `#Group`)
- Person (n:1 `#Person`)
- Description (O text)


## Entity: Object

### Attributes:

- Name (M text)
- Description (O text)


## Entity: Relation

### Attributes:

- From (n:1 `#Person`)
- To (n:1 `#Person`)
- Type (M text)
- Name (O text)


## Entity: Event

### Attributes:

- Type (M)
- From (O timestamp)
- To (O timestamp)
- Duration (O interval)
- Description (O text)


## Entity: Event location

### Attributes:

- Event (n:1 `#Event`)
- Location (n:1 `#Location`)
- Role (O text)


## Entity: Event person

### Attributes:

- Event (n:1 `#Event`)
- Person (n:1 `#Person`)
- Role (O text)


## Entity: Event group

### Attributes:

- Event (n:1 `#Event`)
- Group (n:1 `#Group`)
- Role (O text)


## Entity: Event object

### Attributes:

- Event (n:1 `#Event`)
- Object (n:1 `#Object`)
- Role (O text)
