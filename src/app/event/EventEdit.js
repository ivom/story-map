import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { DateTimePicker, EditScreen, FieldGroup } from '../../shared'
import { updateEvent, useEventEdit } from './event-api'

export default () =>
  <EditScreen
    title="Edit event"
    entityTitle="Event"
    url="/events"
    useResourceEdit={useEventEdit}
    rows={5}
    validationSchema={
      Yup.object({
        type: Yup.string()
          .required(),
        from: Yup.date().nullable(),
        to: Yup.date().nullable(),
        duration: Yup.string(),
        description: Yup.string()
      })
    }
    update={data => updateEvent(data.id, data.version, data)}>

    <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={DateTimePicker} name="from" label="From" sm={[2, 9]}/>
    <FieldGroup as={DateTimePicker} name="to" label="To" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="duration" label="Duration" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </EditScreen>
