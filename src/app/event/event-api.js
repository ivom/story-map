import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  atLeast,
  atMost,
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { dateTimeToApi, temporalFromApi } from '../../shared/utils'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.type).localeCompare(String(b.type)))
}

update(data => ({ ...data, events: data.events || [] }))

export const expandEvent = values => {
  return values
}

export const eventToApi = values => {
  values = dateTimeToApi('from', values)
  values = dateTimeToApi('fromFrom', values)
  values = dateTimeToApi('fromTo', values)
  values = dateTimeToApi('to', values)
  values = dateTimeToApi('toFrom', values)
  values = dateTimeToApi('toTo', values)
  return values
}
export const eventFromApi = values => {
  values = temporalFromApi('from', values)
  values = temporalFromApi('fromFrom', values)
  values = temporalFromApi('fromTo', values)
  values = temporalFromApi('to', values)
  values = temporalFromApi('toFrom', values)
  values = temporalFromApi('toTo', values)
  return values
}

export const listEvents = params => {
  const result = list(params, pageSize, 'events',
    item =>
      numberMatch(params, item, 'id') &&
      caseInsensitiveMatch(params, item, 'type') &&
      atLeast(params, 'fromFrom', item, 'from') &&
      atMost(params, 'fromTo', item, 'from') &&
      atLeast(params, 'toFrom', item, 'to') &&
      atMost(params, 'toTo', item, 'to') &&
      caseInsensitiveMatch(params, item, 'duration') &&
      caseInsensitiveMatch(params, item, 'description')
  )
    .map(expandEvent)
    .map(eventFromApi)
  console.log('listEvents', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEvents = (params, $page = 0, options = {}) =>
  useSWR(['/events',
    qs.stringify(params), $page], () => listEvents({ ...params, $page }), { ...defaultSWROptions, ...options })

const getEvent = id => {
  const result = eventFromApi(expandEvent(getEntity(id, 'events')))
  console.log('getEvent', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEvent = (id, options = {}) =>
  useSWR(`/events/${id}`, optionalGet(id, () => getEvent(id)), { ...defaultSWROptions, ...options })
export const useEventEdit = (id, options = {}) =>
  useEvent(id, { ...editSWROptions, ...options })

export const createEvent = values => {
  const request = eventToApi(values)
  const result = create({
    ...request
  }, 'events', sort)
  console.log('createEvent', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateEvent = (id, version, values) => {
  const request = eventToApi(values)
  console.log('updateEvent', id, version, request)
  modify(id, version, 'events', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchEvent = (id, version, values) => {
  const request = eventToApi(values)
  console.log('patchEvent', id, version, request)
  modify(id, version, 'events', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
