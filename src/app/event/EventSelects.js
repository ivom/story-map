import React from 'react'
import { AsyncSelect } from '../../shared'
import { listEvents, useEvent } from './event-api'
import { entityLabel, useRestored } from '../../shared/utils'
import { formatDateTime } from '../../i18n'

export const eventLabel = data => data && entityLabel(', ',
  data.type,
  formatDateTime(data.from),
  formatDateTime(data.to),
  data.duration,
  data.description)

export const EventSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listEvents({ type: query })}
               getOptionValue={option => option.id}
               getOptionLabel={eventLabel}
               restoredValue={useRestored(name + 'Id', useEvent)}
               name={name}
               {...rest}/>
