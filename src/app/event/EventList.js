import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, DateTimeRangePicker, FieldGroup, ListScreen } from '../../shared'
import { eventFromApi, eventToApi, useEvents } from './event-api'
import { formatDateTime } from '../../i18n'

let searchValuesCache = {
  id: '',
  type: '',
  fromFrom: '',
  fromTo: '',
  toFrom: '',
  toTo: '',
  duration: '',
  description: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Events
        <CreateButton to="/events/new" title="Create new event..."
                      initialValues={eventToApi(searchValuesCache)}/>
      </>
    }
    url="/events"
    useResourceList={useEvents}
    toApi={eventToApi}
    fromApi={eventFromApi}
    searchFormRows={6}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={DateTimeRangePicker} name="from" label="From" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={DateTimeRangePicker} name="to" label="To" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="duration" label="Duration" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={6}
    tableHeader={
      <>
        <th>Id</th>
        <th>Type</th>
        <th>From</th>
        <th>To</th>
        <th>Duration</th>
        <th>Description</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{item.type}</td>
        <td>{formatDateTime(item.from)}</td>
        <td>{formatDateTime(item.to)}</td>
        <td>{item.duration}</td>
        <td>{item.description}</td>
      </>
    }
  />
