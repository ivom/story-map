import React from 'react'
import { AsyncSelect } from '../../shared'
import { listLocationTypes, useLocationType } from './location-type-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const locationTypeLabel = data => data && entityLabel(', ',
  data.name)

export const LocationTypeSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listLocationTypes({ name: query })}
               getOptionValue={option => option.id}
               getOptionLabel={locationTypeLabel}
               restoredValue={useRestored(name + 'Id', useLocationType)}
               name={name}
               {...rest}/>
