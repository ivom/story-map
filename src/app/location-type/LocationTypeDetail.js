import React from 'react'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { useLocationType } from './location-type-api'

export default () =>
  <DetailScreen
    title="Location type detail"
    entityTitle="Location type"
    rows={2}
    useResourceGet={useLocationType}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Type locations" className="mr-3"
                            to={`/locations?typeId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Name" sm={[2, 10]} value={data.name}/>
      </>
    }
  </DetailScreen>
