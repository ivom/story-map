import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import LocationTypeCreate from './LocationTypeCreate'
import LocationTypeEdit from './LocationTypeEdit'
import LocationTypeDetail from './LocationTypeDetail'
import LocationTypeList from './LocationTypeList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <LocationTypeCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <LocationTypeEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <LocationTypeDetail/>
      </Route>
      <Route path={path}>
        <LocationTypeList/>
      </Route>
    </Switch>
  </>
}
