import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.name).localeCompare(String(b.name)))
}

update(data => ({ ...data, locationTypes: data.locationTypes || [] }))

export const expandLocationType = values => {
  return values
}

export const locationTypeToApi = values => {
  return values
}
export const locationTypeFromApi = values => {
  return values
}

export const listLocationTypes = params => {
  const result = list(params, pageSize, 'locationTypes',
    item =>
      numberMatch(params, item, 'id') &&
      caseInsensitiveMatch(params, item, 'name')
  )
    .map(expandLocationType)
    .map(locationTypeFromApi)
  console.log('listLocationTypes', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useLocationTypes = (params, $page = 0, options = {}) =>
  useSWR(['/location-types',
    qs.stringify(params), $page], () => listLocationTypes({ ...params, $page }), { ...defaultSWROptions, ...options })

const getLocationType = id => {
  const result = locationTypeFromApi(expandLocationType(getEntity(id, 'locationTypes')))
  console.log('getLocationType', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useLocationType = (id, options = {}) =>
  useSWR(`/location-types/${id}`, optionalGet(id, () => getLocationType(id)), { ...defaultSWROptions, ...options })
export const useLocationTypeEdit = (id, options = {}) =>
  useLocationType(id, { ...editSWROptions, ...options })

export const createLocationType = values => {
  const request = locationTypeToApi(values)
  const result = create({
    ...request
  }, 'locationTypes', sort)
  console.log('createLocationType', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateLocationType = (id, version, values) => {
  const request = locationTypeToApi(values)
  console.log('updateLocationType', id, version, request)
  modify(id, version, 'locationTypes', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchLocationType = (id, version, values) => {
  const request = locationTypeToApi(values)
  console.log('patchLocationType', id, version, request)
  modify(id, version, 'locationTypes', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
