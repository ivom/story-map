import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { locationTypeFromApi, locationTypeToApi, useLocationTypes } from './location-type-api'

let searchValuesCache = {
  id: '',
  name: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Location types
        <CreateButton to="/location-types/new" title="Create new location type..."
                      initialValues={locationTypeToApi(searchValuesCache)}/>
      </>
    }
    url="/location-types"
    useResourceList={useLocationTypes}
    toApi={locationTypeToApi}
    fromApi={locationTypeFromApi}
    searchFormRows={2}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={2}
    tableHeader={
      <>
        <th>Id</th>
        <th>Name</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{item.name}</td>
      </>
    }
  />
