import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { createLocationType, locationTypeFromApi } from './location-type-api'

export default () =>
  <CreateScreen
    title="Create location type"
    entityTitle="Location type"
    url="/location-types"
    rows={1}
    fromApi={locationTypeFromApi}
    initialValues={{
      name: ''
    }}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required()
      })
    }
    create={createLocationType}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>

  </CreateScreen>
