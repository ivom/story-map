import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { updateLocationType, useLocationTypeEdit } from './location-type-api'

export default () =>
  <EditScreen
    title="Edit location type"
    entityTitle="Location type"
    url="/location-types"
    useResourceEdit={useLocationTypeEdit}
    rows={1}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required()
      })
    }
    update={data => updateLocationType(data.id, data.version, data)}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>

  </EditScreen>
