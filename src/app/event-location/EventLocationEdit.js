import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { LocationSelect } from '../location/LocationSelects'
import { updateEventLocation, useEventLocationEdit } from './event-location-api'

export default () =>
  <EditScreen
    title="Edit event location"
    entityTitle="Event location"
    url="/event-locations"
    useResourceEdit={useEventLocationEdit}
    rows={3}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        location: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    update={data => updateEventLocation(data.id, data.version, data)}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={LocationSelect} name="location" label="Location" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </EditScreen>
