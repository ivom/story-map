import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { LocationSelect } from '../location/LocationSelects'
import { createEventLocation, eventLocationFromApi } from './event-location-api'

export default () =>
  <CreateScreen
    title="Create event location"
    entityTitle="Event location"
    url="/event-locations"
    rows={3}
    fromApi={eventLocationFromApi}
    initialValues={{
      event: '',
      location: '',
      role: ''
    }}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        location: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    create={createEventLocation}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={LocationSelect} name="location" label="Location" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </CreateScreen>
