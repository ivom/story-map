import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { eventLabel, EventSelect } from '../event/EventSelects'
import { locationLabel, LocationSelect } from '../location/LocationSelects'
import { eventLocationFromApi, eventLocationToApi, useEventLocations } from './event-location-api'

let searchValuesCache = {
  id: '',
  event: '',
  location: '',
  role: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Event locations
        <CreateButton to="/event-locations/new" title="Create new event location..."
                      initialValues={eventLocationToApi(searchValuesCache)}/>
      </>
    }
    url="/event-locations"
    useResourceList={useEventLocations}
    toApi={eventLocationToApi}
    fromApi={eventLocationFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={LocationSelect} name="location" label="Location" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Event</th>
        <th>Location</th>
        <th>Role</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{eventLabel(item.event)}</td>
        <td>{locationLabel(item.location)}</td>
        <td>{item.role}</td>
      </>
    }
  />
