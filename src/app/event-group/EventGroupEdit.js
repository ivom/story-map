import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { GroupSelect } from '../group/GroupSelects'
import { updateEventGroup, useEventGroupEdit } from './event-group-api'

export default () =>
  <EditScreen
    title="Edit event group"
    entityTitle="Event group"
    url="/event-groups"
    useResourceEdit={useEventGroupEdit}
    rows={3}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        group: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    update={data => updateEventGroup(data.id, data.version, data)}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </EditScreen>
