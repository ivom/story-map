import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { GroupSelect } from '../group/GroupSelects'
import { createEventGroup, eventGroupFromApi } from './event-group-api'

export default () =>
  <CreateScreen
    title="Create event group"
    entityTitle="Event group"
    url="/event-groups"
    rows={3}
    fromApi={eventGroupFromApi}
    initialValues={{
      event: '',
      group: '',
      role: ''
    }}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        group: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    create={createEventGroup}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={GroupSelect} name="group" label="Group" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </CreateScreen>
