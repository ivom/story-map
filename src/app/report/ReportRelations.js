import React from 'react'
import { Card, ListGroup } from 'react-bootstrap'
import plantumlEncoder from 'plantuml-encoder'
import { get } from '../../api'
import { Link } from 'react-router-dom'

export default () => {
  const trim = value => value ? value.trim() : ''
  const personState = person => person.id
  const personLabel = person => `${trim(person.familyName)} ${trim(person.givenNames)}`
  const data = get()
  const setup = 'hide empty description'
  const peopleWithRelationIds = new Set()
  data.relations.forEach(relation => {
    peopleWithRelationIds.add(relation.fromId)
    peopleWithRelationIds.add(relation.toId)
  })
  const states = data.people
    .filter(person => peopleWithRelationIds.has(person.id))
    .map(person => ({
      code: personState(person),
      label: personLabel(person)
    }))
    .map(({ code, label }) => `\nstate "${label}" as ${code}`)
  const relations = data.relations
    .map(relation => ({
      from: relation.fromId,
      to: relation.toId,
      label: relation.type + (relation.name ? ' ' + relation.name : '')
    }))
    .map(({ from, to, label }) => `\n${from} --> ${to} : ${label}`)
  const content = setup + states.join('') + relations.join('')

  const url = 'http://www.plantuml.com/plantuml/svg/' + plantumlEncoder.encode(content)

  return <>
    <h2>
      Relations report
    </h2>

    <Card className="mb-3">
      <Card.Body>
        <Card.Title>
          People with no relations
        </Card.Title>
        <Card.Text>
          {peopleWithRelationIds.size < data.people.length
            ? <span>These people are not related to any other people.</span>
            : <span className="text-muted">There are no people without relations.</span>
          }
        </Card.Text>
      </Card.Body>
      <ListGroup variant="flush">
        {data.people
          .filter(person => !peopleWithRelationIds.has(person.id))
          .map(person =>
            <ListGroup.Item key={person.id}
                            action as={Link} to={`/people/${person.id}`}>
            <span className="mr-4">
              {person.familyName} {person.givenNames}
            </span>
              <em>{person.occupation}</em>
            </ListGroup.Item>
          )}
      </ListGroup>
    </Card>

    <Card className="mb-3">
      <Card.Body>
        <Card.Title>
          Relations between people
        </Card.Title>
        <Card.Text>
          {states.length
            ? <img src={url} alt="Relations between people"/>
            : <span className="text-muted">There are no people yet.</span>
          }
        </Card.Text>
      </Card.Body>
    </Card>
  </>
}
