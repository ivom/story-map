import React from 'react'
import { Badge, Button, Card, ListGroup } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import { get } from '../../api'

const getPersonGroups = (data, person) =>
  data.groupMembers
    .filter(({ personId }) => personId === person.id)
    .map(({ groupId }) =>
      data.groups
        .filter(({ id }) => id === groupId)[0]
    )

const getPeopleGroups = data => {
  return data.people
    .map(person => ({ ...person, groups: getPersonGroups(data, person) }))
}

export default () => {
  const history = useHistory()

  const peopleGroups = getPeopleGroups(get())
  const noGroupPeople = peopleGroups
    .filter(({ groups }) => groups.length === 0)
  const multipleGroupsPeople = peopleGroups
    .filter(({ groups }) => groups.length > 1)
  multipleGroupsPeople.sort((a, b) => b.groups.length - a.groups.length)

  const goToGroup = id => event => {
    event.preventDefault()
    history.push(`/groups/${id}`)
  }

  return <>
    <h2>
      Groups report
    </h2>

    <Card className="mb-3">
      <Card.Body>
        <Card.Title>
          People in no group
        </Card.Title>
        <Card.Text>
          {noGroupPeople.length
            ? <span>These people have not been assigned to any group.</span>
            : <span className="text-muted">There are no people not assigned to any group.</span>
          }
        </Card.Text>
      </Card.Body>
      <ListGroup variant="flush">
        {noGroupPeople.map(person =>
          <ListGroup.Item key={person.id}
                          action as={Link} to={`/people/${person.id}`}>
            <span className="mr-4">
              {person.familyName} {person.givenNames}
            </span>
            <em>{person.occupation}</em>
          </ListGroup.Item>
        )}
      </ListGroup>
    </Card>

    <Card>
      <Card.Body>
        <Card.Title>
          People in multiple groups
        </Card.Title>
        <Card.Text>
          {multipleGroupsPeople.length
            ? <span>These people have been assigned to multiple groups.</span>
            : <span className="text-muted">There are no people assigned to multiple groups.</span>
          }
        </Card.Text>
      </Card.Body>
      <ListGroup variant="flush">
        {multipleGroupsPeople.map(person =>
          <ListGroup.Item key={person.id}
                          action as={Link} to={`/people/${person.id}`}>
            <span className="mr-3">
              {person.familyName} {person.givenNames}
            </span>
            {person.groups
              .map(group =>
                <Button key={group.id}
                        variant="outline-secondary" size="sm"
                        className="mx-2"
                        onClick={goToGroup(group.id)}>
                  {group.name}
                </Button>
              )}
            <h5 className="d-inline-block float-right">
              <Badge variant="secondary">{person.groups.length}</Badge>
            </h5>
          </ListGroup.Item>
        )}
      </ListGroup>
    </Card>
  </>
}
