import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { locationTypeLabel, LocationTypeSelect } from '../location-type/LocationTypeSelects'
import { locationLabel, LocationSelect } from '../location/LocationSelects'
import { locationFromApi, locationToApi, useLocations } from './location-api'

let searchValuesCache = {
  id: '',
  type: '',
  value: '',
  parent: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Locations
        <CreateButton to="/locations/new" title="Create new location..."
                      initialValues={locationToApi(searchValuesCache)}/>
      </>
    }
    url="/locations"
    useResourceList={useLocations}
    toApi={locationToApi}
    fromApi={locationFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={LocationTypeSelect} name="type" label="Type" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="value" label="Value" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={LocationSelect} name="parent" label="Parent" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Type</th>
        <th>Value</th>
        <th>Parent</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{locationTypeLabel(item.type)}</td>
        <td>{item.value}</td>
        <td>{locationLabel(item.parent)}</td>
      </>
    }
  />
