import React from 'react'
import { locationTypeLabel } from '../location-type/LocationTypeSelects'
import { AsyncSelect } from '../../shared'
import { listLocations, useLocation } from './location-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const locationLabel = data => data && entityLabel(', ',
  locationTypeLabel(data.type),
  data.value,
  locationLabel(data.parent))

export const LocationSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listLocations({ type: query })}
               getOptionValue={option => option.id}
               getOptionLabel={locationLabel}
               restoredValue={useRestored(name + 'Id', useLocation)}
               name={name}
               {...rest}/>
