import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.name).localeCompare(String(b.name)))
}

update(data => ({ ...data, groups: data.groups || [] }))

export const expandGroup = values => {
  if (values) {
    values = expand(values, 'parentId', 'parent', 'groups')
    values.parent = expandGroup(values.parent)
  }
  return values
}

export const groupToApi = values => {
  values = collapse(values, 'parent', 'id', 'parentId')
  return values
}
export const groupFromApi = values => {
  values = restore(values, 'parentId', 'parent', 'id')
  return values
}

export const listGroups = params => {
  const result = list(params, pageSize, 'groups',
    item =>
      numberMatch(params, item, 'id') &&
      caseInsensitiveMatch(params, item, 'name') &&
      numberMatch(params, item, 'parentId')
  )
    .map(expandGroup)
    .map(groupFromApi)
  console.log('listGroups', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useGroups = (params, $page = 0, options = {}) =>
  useSWR(['/groups',
    qs.stringify(params), $page], () => listGroups({ ...params, $page }), { ...defaultSWROptions, ...options })

const getGroup = id => {
  const result = groupFromApi(expandGroup(getEntity(id, 'groups')))
  console.log('getGroup', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useGroup = (id, options = {}) =>
  useSWR(`/groups/${id}`, optionalGet(id, () => getGroup(id)), { ...defaultSWROptions, ...options })
export const useGroupEdit = (id, options = {}) =>
  useGroup(id, { ...editSWROptions, ...options })

export const createGroup = values => {
  const request = groupToApi(values)
  const result = create({
    ...request
  }, 'groups', sort)
  console.log('createGroup', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateGroup = (id, version, values) => {
  const request = groupToApi(values)
  console.log('updateGroup', id, version, request)
  modify(id, version, 'groups', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchGroup = (id, version, values) => {
  const request = groupToApi(values)
  console.log('patchGroup', id, version, request)
  modify(id, version, 'groups', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
