import React from 'react'
import { AsyncSelect } from '../../shared'
import { listGroups, useGroup } from './group-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const groupLabel = data => data && entityLabel(', ',
  data.name,
  groupLabel(data.parent))

export const GroupSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listGroups({ name: query })}
               getOptionValue={option => option.id}
               getOptionLabel={groupLabel}
               restoredValue={useRestored(name + 'Id', useGroup)}
               name={name}
               {...rest}/>
