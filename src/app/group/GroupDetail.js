import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { groupLabel } from '../group/GroupSelects'
import { useGroup } from './group-api'

export default () =>
  <DetailScreen
    title="Group detail"
    entityTitle="Group"
    rows={3}
    useResourceGet={useGroup}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Parent groups" className="mr-3"
                            to={`/groups?parentId=${data.id}`}/>
          <NavigationButton label="Group group members" className="mr-3"
                            to={`/group-members?groupId=${data.id}`}/>
          <NavigationButton label="Group event groups" className="mr-3"
                            to={`/event-groups?groupId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Name" sm={[2, 10]} value={data.name}/>
        <StaticGroup label="Parent" sm={[2, 10]}>
          <Link to={`/groups/${data.parent?.id}`}>
            {groupLabel(data.parent)}
          </Link>
        </StaticGroup>
      </>
    }
  </DetailScreen>
