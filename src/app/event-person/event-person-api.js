import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandEvent } from '../event/event-api'
import { expandPerson } from '../person/person-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.event?.type).localeCompare(String(b.event?.type)))
}

update(data => ({ ...data, eventPeople: data.eventPeople || [] }))

export const expandEventPerson = values => {
  if (values) {
    values = expand(values, 'eventId', 'event', 'events')
    values.event = expandEvent(values.event)
    values = expand(values, 'personId', 'person', 'people')
    values.person = expandPerson(values.person)
  }
  return values
}

export const eventPersonToApi = values => {
  values = collapse(values, 'event', 'id', 'eventId')
  values = collapse(values, 'person', 'id', 'personId')
  return values
}
export const eventPersonFromApi = values => {
  values = restore(values, 'eventId', 'event', 'id')
  values = restore(values, 'personId', 'person', 'id')
  return values
}

export const listEventPeople = params => {
  const result = list(params, pageSize, 'eventPeople',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'eventId') &&
      numberMatch(params, item, 'personId') &&
      caseInsensitiveMatch(params, item, 'role')
  )
    .map(expandEventPerson)
    .map(eventPersonFromApi)
  console.log('listEventPeople', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventPeople = (params, $page = 0, options = {}) =>
  useSWR(['/event-people',
    qs.stringify(params), $page], () => listEventPeople({ ...params, $page }), { ...defaultSWROptions, ...options })

const getEventPerson = id => {
  const result = eventPersonFromApi(expandEventPerson(getEntity(id, 'eventPeople')))
  console.log('getEventPerson', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useEventPerson = (id, options = {}) =>
  useSWR(`/event-people/${id}`, optionalGet(id, () => getEventPerson(id)), { ...defaultSWROptions, ...options })
export const useEventPersonEdit = (id, options = {}) =>
  useEventPerson(id, { ...editSWROptions, ...options })

export const createEventPerson = values => {
  const request = eventPersonToApi(values)
  const result = create({
    ...request
  }, 'eventPeople', sort)
  console.log('createEventPerson', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateEventPerson = (id, version, values) => {
  const request = eventPersonToApi(values)
  console.log('updateEventPerson', id, version, request)
  modify(id, version, 'eventPeople', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchEventPerson = (id, version, values) => {
  const request = eventPersonToApi(values)
  console.log('patchEventPerson', id, version, request)
  modify(id, version, 'eventPeople', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
