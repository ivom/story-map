import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import EventPersonCreate from './EventPersonCreate'
import EventPersonEdit from './EventPersonEdit'
import EventPersonDetail from './EventPersonDetail'
import EventPersonList from './EventPersonList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <EventPersonCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <EventPersonEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <EventPersonDetail/>
      </Route>
      <Route path={path}>
        <EventPersonList/>
      </Route>
    </Switch>
  </>
}
