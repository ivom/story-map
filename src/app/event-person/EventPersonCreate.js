import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { PersonSelect } from '../person/PersonSelects'
import { createEventPerson, eventPersonFromApi } from './event-person-api'

export default () =>
  <CreateScreen
    title="Create event person"
    entityTitle="Event person"
    url="/event-people"
    rows={3}
    fromApi={eventPersonFromApi}
    initialValues={{
      event: '',
      person: '',
      role: ''
    }}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        person: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    create={createEventPerson}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </CreateScreen>
