import React from 'react'
import { eventLabel } from '../event/EventSelects'
import { personLabel } from '../person/PersonSelects'
import { AsyncSelect } from '../../shared'
import { listEventPeople, useEventPerson } from './event-person-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const eventPersonLabel = data => data && entityLabel(', ',
  eventLabel(data.event),
  personLabel(data.person),
  data.role)

export const EventPersonSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listEventPeople({ event: query })}
               getOptionValue={option => option.id}
               getOptionLabel={eventPersonLabel}
               restoredValue={useRestored(name + 'Id', useEventPerson)}
               name={name}
               {...rest}/>
