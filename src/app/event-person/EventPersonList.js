import React from 'react'
import { Form } from 'react-bootstrap'
import { CreateButton, FieldGroup, ListScreen } from '../../shared'
import { eventLabel, EventSelect } from '../event/EventSelects'
import { personLabel, PersonSelect } from '../person/PersonSelects'
import { eventPersonFromApi, eventPersonToApi, useEventPeople } from './event-person-api'

let searchValuesCache = {
  id: '',
  event: '',
  person: '',
  role: ''
}

export default () =>
  <ListScreen
    searchValuesCache={searchValuesCache}
    setSearchValuesCache={values => searchValuesCache = values}
    title={
      <>
        Event people
        <CreateButton to="/event-people/new" title="Create new event person..."
                      initialValues={eventPersonToApi(searchValuesCache)}/>
      </>
    }
    url="/event-people"
    useResourceList={useEventPeople}
    toApi={eventPersonToApi}
    fromApi={eventPersonFromApi}
    searchFormRows={4}
    searchFormContent={
      <>
        <FieldGroup as={Form.Control} name="id" label="Id" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={PersonSelect} name="person" label="Person" sm={[2, 9]} isValid={false}/>
        <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]} isValid={false}/>
      </>
    }
    columns={4}
    tableHeader={
      <>
        <th>Id</th>
        <th>Event</th>
        <th>Person</th>
        <th>Role</th>
      </>
    }
    tablePageContent={
      item => <>
        <td>{item.id}</td>
        <td>{eventLabel(item.event)}</td>
        <td>{personLabel(item.person)}</td>
        <td>{item.role}</td>
      </>
    }
  />
