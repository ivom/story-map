import React from 'react'
import { Link } from 'react-router-dom'
import { Alert, Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useFirstMountState } from 'react-use'
import encoder from 'plantuml-encoder-decoder'
import { update } from '../../api'

export default () => {
  const isFirstMount = useFirstMountState()

  if (isFirstMount) {
    const encodedData = window.location.hash.substring('#/restore-data/'.length)
    const decodedData = encoder.decode(encodedData)
    update(() => JSON.parse(decodedData))
  }

  return <>
    <h2>Restore data</h2>
    <Alert variant="success">
      <Alert.Heading>
        Success
      </Alert.Heading>
      Data has been successfully restored.
    </Alert>
    <p>
      <Link to="/">
        <Button variant="outline-secondary">
          Continue to home page&nbsp;
          <FontAwesomeIcon icon={'chevron-right'}/>
        </Button>
      </Link>
    </p>
  </>
}
