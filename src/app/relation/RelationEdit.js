import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { PersonSelect } from '../person/PersonSelects'
import { updateRelation, useRelationEdit } from './relation-api'

export default () =>
  <EditScreen
    title="Edit relation"
    entityTitle="Relation"
    url="/relations"
    useResourceEdit={useRelationEdit}
    rows={4}
    validationSchema={
      Yup.object({
        from: Yup.object().nullable()
          .required(),
        to: Yup.object().nullable()
          .required(),
        type: Yup.string()
          .required(),
        name: Yup.string()
      })
    }
    update={data => updateRelation(data.id, data.version, data)}>

    <FieldGroup as={PersonSelect} name="from" label="From" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="to" label="To" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]}/>

  </EditScreen>
