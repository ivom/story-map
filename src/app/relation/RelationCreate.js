import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { PersonSelect } from '../person/PersonSelects'
import { createRelation, relationFromApi } from './relation-api'

export default () =>
  <CreateScreen
    title="Create relation"
    entityTitle="Relation"
    url="/relations"
    rows={4}
    fromApi={relationFromApi}
    initialValues={{
      from: '',
      to: '',
      type: '',
      name: ''
    }}
    validationSchema={
      Yup.object({
        from: Yup.object().nullable()
          .required(),
        to: Yup.object().nullable()
          .required(),
        type: Yup.string()
          .required(),
        name: Yup.string()
      })
    }
    create={createRelation}>

    <FieldGroup as={PersonSelect} name="from" label="From" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={PersonSelect} name="to" label="To" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="type" label="Type" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]}/>

  </CreateScreen>
