import React from 'react'
import { personLabel } from '../person/PersonSelects'
import { AsyncSelect } from '../../shared'
import { listRelations, useRelation } from './relation-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const relationLabel = data => data && entityLabel(', ',
  personLabel(data.from),
  personLabel(data.to),
  data.type,
  data.name)

export const RelationSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listRelations({ from: query })}
               getOptionValue={option => option.id}
               getOptionLabel={relationLabel}
               restoredValue={useRestored(name + 'Id', useRelation)}
               name={name}
               {...rest}/>
