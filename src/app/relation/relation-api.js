import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  expand,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'
import { collapse, restore } from '../../shared/utils'
import { expandPerson } from '../person/person-api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.from?.familyName).localeCompare(String(b.from?.familyName)))
}

update(data => ({ ...data, relations: data.relations || [] }))

export const expandRelation = values => {
  if (values) {
    values = expand(values, 'fromId', 'from', 'people')
    values.from = expandPerson(values.from)
    values = expand(values, 'toId', 'to', 'people')
    values.to = expandPerson(values.to)
  }
  return values
}

export const relationToApi = values => {
  values = collapse(values, 'from', 'id', 'fromId')
  values = collapse(values, 'to', 'id', 'toId')
  return values
}
export const relationFromApi = values => {
  values = restore(values, 'fromId', 'from', 'id')
  values = restore(values, 'toId', 'to', 'id')
  return values
}

export const listRelations = params => {
  const result = list(params, pageSize, 'relations',
    item =>
      numberMatch(params, item, 'id') &&
      numberMatch(params, item, 'fromId') &&
      numberMatch(params, item, 'toId') &&
      caseInsensitiveMatch(params, item, 'type') &&
      caseInsensitiveMatch(params, item, 'name')
  )
    .map(expandRelation)
    .map(relationFromApi)
  console.log('listRelations', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useRelations = (params, $page = 0, options = {}) =>
  useSWR(['/relations',
    qs.stringify(params), $page], () => listRelations({ ...params, $page }), { ...defaultSWROptions, ...options })

const getRelation = id => {
  const result = relationFromApi(expandRelation(getEntity(id, 'relations')))
  console.log('getRelation', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useRelation = (id, options = {}) =>
  useSWR(`/relations/${id}`, optionalGet(id, () => getRelation(id)), { ...defaultSWROptions, ...options })
export const useRelationEdit = (id, options = {}) =>
  useRelation(id, { ...editSWROptions, ...options })

export const createRelation = values => {
  const request = relationToApi(values)
  const result = create({
    ...request
  }, 'relations', sort)
  console.log('createRelation', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateRelation = (id, version, values) => {
  const request = relationToApi(values)
  console.log('updateRelation', id, version, request)
  modify(id, version, 'relations', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchRelation = (id, version, values) => {
  const request = relationToApi(values)
  console.log('patchRelation', id, version, request)
  modify(id, version, 'relations', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
