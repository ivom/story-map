import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import EventObjectCreate from './EventObjectCreate'
import EventObjectEdit from './EventObjectEdit'
import EventObjectDetail from './EventObjectDetail'
import EventObjectList from './EventObjectList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <EventObjectCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <EventObjectEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <EventObjectDetail/>
      </Route>
      <Route path={path}>
        <EventObjectList/>
      </Route>
    </Switch>
  </>
}
