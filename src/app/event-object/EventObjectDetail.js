import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { eventLabel } from '../event/EventSelects'
import { objectLabel } from '../object/ObjectSelects'
import { useEventObject } from './event-object-api'

export default () =>
  <DetailScreen
    title="Event object detail"
    entityTitle="Event object"
    rows={4}
    useResourceGet={useEventObject}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Event" sm={[2, 10]}>
          <Link to={`/events/${data.event?.id}`}>
            {eventLabel(data.event)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Object" sm={[2, 10]}>
          <Link to={`/objects/${data.object?.id}`}>
            {objectLabel(data.object)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Role" sm={[2, 10]} value={data.role}/>
      </>
    }
  </DetailScreen>
