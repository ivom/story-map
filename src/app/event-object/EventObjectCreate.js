import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { ObjectSelect } from '../object/ObjectSelects'
import { createEventObject, eventObjectFromApi } from './event-object-api'

export default () =>
  <CreateScreen
    title="Create event object"
    entityTitle="Event object"
    url="/event-objects"
    rows={3}
    fromApi={eventObjectFromApi}
    initialValues={{
      event: '',
      object: '',
      role: ''
    }}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        object: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    create={createEventObject}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={ObjectSelect} name="object" label="Object" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </CreateScreen>
