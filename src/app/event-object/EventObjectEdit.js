import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { EventSelect } from '../event/EventSelects'
import { ObjectSelect } from '../object/ObjectSelects'
import { updateEventObject, useEventObjectEdit } from './event-object-api'

export default () =>
  <EditScreen
    title="Edit event object"
    entityTitle="Event object"
    url="/event-objects"
    useResourceEdit={useEventObjectEdit}
    rows={3}
    validationSchema={
      Yup.object({
        event: Yup.object().nullable()
          .required(),
        object: Yup.object().nullable()
          .required(),
        role: Yup.string()
      })
    }
    update={data => updateEventObject(data.id, data.version, data)}>

    <FieldGroup as={EventSelect} name="event" label="Event" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={ObjectSelect} name="object" label="Object" sm={[2, 9]} required/>
    <FieldGroup as={Form.Control} name="role" label="Role" sm={[2, 9]}/>

  </EditScreen>
