import React from 'react'
import { DetailScreen, EditButton, NavigationButton, StaticGroup } from '../../shared'
import { useObject } from './object-api'

export default () =>
  <DetailScreen
    title="Object detail"
    entityTitle="Object"
    rows={3}
    useResourceGet={useObject}
    buttons={
      (data) =>
        <>
          <EditButton className="mr-3" autoFocus/>

          <NavigationButton label="Object event objects" className="mr-3"
                            to={`/event-objects?objectId=${data.id}`}/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Name" sm={[2, 10]} value={data.name}/>
        <StaticGroup label="Description" sm={[2, 10]} value={data.description}/>
      </>
    }
  </DetailScreen>
