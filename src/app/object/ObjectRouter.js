import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import ObjectCreate from './ObjectCreate'
import ObjectEdit from './ObjectEdit'
import ObjectDetail from './ObjectDetail'
import ObjectList from './ObjectList'

export default () => {
  const { path } = useRouteMatch()

  return <>
    <Switch>
      <Route path={`${path}/new`}>
        <ObjectCreate/>
      </Route>
      <Route path={`${path}/:id/edit`}>
        <ObjectEdit/>
      </Route>
      <Route path={`${path}/:id`}>
        <ObjectDetail/>
      </Route>
      <Route path={path}>
        <ObjectList/>
      </Route>
    </Switch>
  </>
}
