import React from 'react'
import { AsyncSelect } from '../../shared'
import { listObjects, useObject } from './object-api'
import { entityLabel, useRestored } from '../../shared/utils'

export const objectLabel = data => data && entityLabel(', ',
  data.name,
  data.description)

export const ObjectSelect = ({ name, ...rest }) =>
  <AsyncSelect searchFn={query => listObjects({ name: query })}
               getOptionValue={option => option.id}
               getOptionLabel={objectLabel}
               restoredValue={useRestored(name + 'Id', useObject)}
               name={name}
               {...rest}/>
