import useSWR from 'swr'
import qs from 'qs'
import { defaultPageSize } from '../../constants'
import {
  caseInsensitiveMatch,
  create,
  defaultSWROptions,
  delay,
  editSWROptions,
  getEntity,
  list,
  modify,
  numberMatch,
  optionalGet,
  update
} from '../../api'

const pageSize = defaultPageSize
const sort = data => {
  data.sort((a, b) => String(a.name).localeCompare(String(b.name)))
}

update(data => ({ ...data, objects: data.objects || [] }))

export const expandObject = values => {
  return values
}

export const objectToApi = values => {
  return values
}
export const objectFromApi = values => {
  return values
}

export const listObjects = params => {
  const result = list(params, pageSize, 'objects',
    item =>
      numberMatch(params, item, 'id') &&
      caseInsensitiveMatch(params, item, 'name') &&
      caseInsensitiveMatch(params, item, 'description')
  )
    .map(expandObject)
    .map(objectFromApi)
  console.log('listObjects', params, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useObjects = (params, $page = 0, options = {}) =>
  useSWR(['/objects',
    qs.stringify(params), $page], () => listObjects({ ...params, $page }), { ...defaultSWROptions, ...options })

const getObject = id => {
  const result = objectFromApi(expandObject(getEntity(id, 'objects')))
  console.log('getObject', id, '=>', result)
  return Promise.resolve(result).then(delay)
}
export const useObject = (id, options = {}) =>
  useSWR(`/objects/${id}`, optionalGet(id, () => getObject(id)), { ...defaultSWROptions, ...options })
export const useObjectEdit = (id, options = {}) =>
  useObject(id, { ...editSWROptions, ...options })

export const createObject = values => {
  const request = objectToApi(values)
  const result = create({
    ...request
  }, 'objects', sort)
  console.log('createObject', request, '=>', result)
  return Promise.resolve(result).then(delay)
}

export const updateObject = (id, version, values) => {
  const request = objectToApi(values)
  console.log('updateObject', id, version, request)
  modify(id, version, 'objects', sort,
    (id, version) => ({ ...request, id, version }))
  return Promise.resolve().then(delay)
}

export const patchObject = (id, version, values) => {
  const request = objectToApi(values)
  console.log('patchObject', id, version, request)
  modify(id, version, 'objects', sort,
    (id, version, oldValues) => ({ ...oldValues, ...request, id, version }))
  return Promise.resolve().then(delay)
}
