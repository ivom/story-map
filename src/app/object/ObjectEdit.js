import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import { updateObject, useObjectEdit } from './object-api'

export default () =>
  <EditScreen
    title="Edit object"
    entityTitle="Object"
    url="/objects"
    useResourceEdit={useObjectEdit}
    rows={2}
    validationSchema={
      Yup.object({
        name: Yup.string()
          .required(),
        description: Yup.string()
      })
    }
    update={data => updateObject(data.id, data.version, data)}>

    <FieldGroup as={Form.Control} name="name" label="Name" sm={[2, 9]} required autoFocus/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </EditScreen>
