import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { CreateScreen, FieldGroup } from '../../shared'
import PersonSexSelect from './PersonSexSelect'
import { createPerson, personFromApi } from './person-api'

export default () =>
  <CreateScreen
    title="Create person"
    entityTitle="Person"
    url="/people"
    rows={5}
    fromApi={personFromApi}
    initialValues={{
      familyName: '',
      givenNames: '',
      sex: '',
      occupation: '',
      description: ''
    }}
    validationSchema={
      Yup.object({
        familyName: Yup.string(),
        givenNames: Yup.string(),
        sex: Yup.string(),
        occupation: Yup.string(),
        description: Yup.string()
      })
    }
    create={createPerson}>

    <FieldGroup as={Form.Control} name="familyName" label="Family name" sm={[2, 9]} autoFocus/>
    <FieldGroup as={Form.Control} name="givenNames" label="Given names" sm={[2, 9]}/>
    <FieldGroup as={PersonSexSelect} name="sex" label="Sex" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="occupation" label="Occupation" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </CreateScreen>
