import React from 'react'
import { Form } from 'react-bootstrap'
import * as Yup from 'yup'
import { EditScreen, FieldGroup } from '../../shared'
import PersonSexSelect from './PersonSexSelect'
import { updatePerson, usePersonEdit } from './person-api'

export default () =>
  <EditScreen
    title="Edit person"
    entityTitle="Person"
    url="/people"
    useResourceEdit={usePersonEdit}
    rows={5}
    validationSchema={
      Yup.object({
        familyName: Yup.string(),
        givenNames: Yup.string(),
        sex: Yup.string(),
        occupation: Yup.string(),
        description: Yup.string()
      })
    }
    update={data => updatePerson(data.id, data.version, data)}>

    <FieldGroup as={Form.Control} name="familyName" label="Family name" sm={[2, 9]} autoFocus/>
    <FieldGroup as={Form.Control} name="givenNames" label="Given names" sm={[2, 9]}/>
    <FieldGroup as={PersonSexSelect} name="sex" label="Sex" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="occupation" label="Occupation" sm={[2, 9]}/>
    <FieldGroup as={Form.Control} name="description" label="Description" sm={[2, 9]}/>

  </EditScreen>
