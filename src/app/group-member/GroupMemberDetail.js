import React from 'react'
import { Link } from 'react-router-dom'
import { DetailScreen, EditButton, StaticGroup } from '../../shared'
import { groupLabel } from '../group/GroupSelects'
import { personLabel } from '../person/PersonSelects'
import { useGroupMember } from './group-member-api'

export default () =>
  <DetailScreen
    title="Group member detail"
    entityTitle="Group member"
    rows={4}
    useResourceGet={useGroupMember}
    buttons={
      () =>
        <>
          <EditButton className="mr-3" autoFocus/>
        </>
    }>
    {data =>
      <>
        <StaticGroup label="Id" sm={[2, 10]} value={data.id}/>
        <StaticGroup label="Group" sm={[2, 10]}>
          <Link to={`/groups/${data.group?.id}`}>
            {groupLabel(data.group)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Person" sm={[2, 10]}>
          <Link to={`/people/${data.person?.id}`}>
            {personLabel(data.person)}
          </Link>
        </StaticGroup>
        <StaticGroup label="Description" sm={[2, 10]} value={data.description}/>
      </>
    }
  </DetailScreen>
