import React from 'react'
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import './App.css'
import Header from './layout/Header'
import Footer from './layout/Footer'
import Home from './layout/Home'
import PersonRouter from './app/person/PersonRouter'
import LocationRouter from './app/location/LocationRouter'
import LocationTypeRouter from './app/location-type/LocationTypeRouter'
import GroupRouter from './app/group/GroupRouter'
import GroupMemberRouter from './app/group-member/GroupMemberRouter'
import ObjectRouter from './app/object/ObjectRouter'
import RelationRouter from './app/relation/RelationRouter'
import EventRouter from './app/event/EventRouter'
import EventLocationRouter from './app/event-location/EventLocationRouter'
import EventPersonRouter from './app/event-person/EventPersonRouter'
import EventGroupRouter from './app/event-group/EventGroupRouter'
import EventObjectRouter from './app/event-object/EventObjectRouter'
import RestoreData from './app/restore/RestoreData'
import ReportGroups from './app/report/ReportGroups'
import ReportRelations from './app/report/ReportRelations'

export default () =>
  <Router>
    <Header/>
    <Container id="app-container" fluid>
      <Switch>
        <Route path="/people">
          <PersonRouter/>
        </Route>
        <Route path="/locations">
          <LocationRouter/>
        </Route>
        <Route path="/location-types">
          <LocationTypeRouter/>
        </Route>
        <Route path="/groups">
          <GroupRouter/>
        </Route>
        <Route path="/group-members">
          <GroupMemberRouter/>
        </Route>
        <Route path="/objects">
          <ObjectRouter/>
        </Route>
        <Route path="/relations">
          <RelationRouter/>
        </Route>
        <Route path="/events">
          <EventRouter/>
        </Route>
        <Route path="/event-locations">
          <EventLocationRouter/>
        </Route>
        <Route path="/event-people">
          <EventPersonRouter/>
        </Route>
        <Route path="/event-groups">
          <EventGroupRouter/>
        </Route>
        <Route path="/event-objects">
          <EventObjectRouter/>
        </Route>
        <Route path="/restore-data">
          <RestoreData/>
        </Route>
        <Route path="/reports/groups">
          <ReportGroups/>
        </Route>
        <Route path="/reports/relations">
          <ReportRelations/>
        </Route>
        <Route path="/">
          <Home/>
        </Route>
      </Switch>
      <Footer/>
    </Container>
  </Router>
