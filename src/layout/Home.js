import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, Card } from 'react-bootstrap'
import { useCopyToClipboard } from 'react-use'
import encoder from 'plantuml-encoder-decoder'
import { get, update } from '../api'

export default () => {
  const [, copyToClipboard] = useCopyToClipboard()
  const [copied, setCopied] = useState(false)
  const [deleting, setDeleting] = useState(false)

  const data = JSON.stringify(get())
  const encodedData = encoder.encode(data)
  const restoreDataLink = `${window.location.href}restore-data/${encodedData}`

  return <>
    <h2>Home</h2>
    <p>
      Welcome to <strong>Story Map</strong>: an application to record the properties of a story.
    </p>
    <p>
      Capture the facts in any story to:
    </p>
    <ul>
      <li>Wrap your head around it.</li>
      <li>Connect the dots.</li>
      <li>Crack a whodunnit mystery.</li>
      <li>Convey the gist of the story.</li>
    </ul>
    <p>
      Start by <Link to="/people">recording the people</Link>.
    </p>

    <Card className="mb-3">
      <Card.Body>
        <Card.Title>
          <h4>Save & share</h4>
        </Card.Title>
        <Card.Text>
          Save or send this link to restore your current data or share it with your friends:
        </Card.Text>
        <Card.Text className="bg-light p-2">
          <samp>
            {restoreDataLink}
          </samp>
        </Card.Text>
        <Card.Text>
          <Button variant="outline-secondary"
                  className="mr-3"
                  onClick={() => {
                    copyToClipboard(restoreDataLink)
                    setCopied(true)
                  }}>
            Copy
          </Button>
          {copied && <em className="text-muted">Copied.</em>}
        </Card.Text>
      </Card.Body>
    </Card>

    <Card bg="danger" text="white" className="mb-3">
      <Card.Header>
        Danger zone
      </Card.Header>
      <Card.Body className="text-dark bg-white">
        <Card.Title>
          <h4>Delete all data</h4>
        </Card.Title>
        <Card.Text>
          Once you delete the data, there is no going back. Please be certain.
        </Card.Text>
        <Card.Text>
          <Button variant="warning" className="mr-5"
                  onClick={() => {
                    setDeleting(!deleting)
                  }}>
            Delete all data
          </Button>
          {deleting &&
          <Button variant="danger"
                  onClick={() => {
                    update(() => ({}))
                    setDeleting(false)
                    window.location.reload()
                  }}>
            Confirm delete
          </Button>
          }
        </Card.Text>
      </Card.Body>
    </Card>
  </>
}
